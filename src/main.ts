import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as config from 'config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const serverConfig = config.get('server');

  const logger = new Logger('bootstrap');

  const app = await NestFactory.create(AppModule);

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Task management')
    .setDescription('Task management api description')
    .setVersion('1.0')
    .addBearerAuth()
    .addTag('tasks')
    .addTag('auth')
    .build()

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('swagger-ui', app, document);

  app.enableCors();
  const port = process.env.PORT || serverConfig.port;
  await app.listen(port);

  logger.log(`Applications listetning on port ${port}`)
}
bootstrap();
