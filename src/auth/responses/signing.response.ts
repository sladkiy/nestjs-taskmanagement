import { ApiProperty } from "@nestjs/swagger";

export class SignIngRespose {
    @ApiProperty()
    accessToken: string;
}