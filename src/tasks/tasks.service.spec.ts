import { NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { User } from '../auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';
import { TasksService } from './tasks.service';

const mockTaskrepository = (): Partial<TaskRepository> => ({
    getTasks: jest.fn(),
    findOne: jest.fn(),
    createTask: jest.fn(),
    delete: jest.fn(),
});

const mockUser = new User();
mockUser.id = 12;

describe('TasksService', () => {
    let tasksService: TasksService;
    let taskRepository: TaskRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                TasksService,
                { provide: TaskRepository, useFactory: mockTaskrepository }
            ]
        }).compile();

        tasksService = module.get<TasksService>(TasksService);
        taskRepository = module.get<TaskRepository>(TaskRepository);
    })

    describe('getTasks', () => {
        it ('gets all tasks from repository', async () => {
            (taskRepository.getTasks as jest.Mock).mockResolvedValue('someValue');
            const filterDto: GetTasksFilterDto = { status: TaskStatus.IN_PROGRESS, search: 'search' };


            expect(taskRepository.getTasks).not.toHaveBeenCalled();

            const result = await tasksService.getTasks(filterDto, mockUser)
            expect(taskRepository.getTasks).toHaveBeenCalled();
            expect(result).toEqual('someValue')
        })
    })

    describe('getTaskById', () => {
        it('calls taskRepository.findOne() and succesfully retreive and return the task', async () => {
            const testTask = new Task();
            testTask.title = 'test title';
            testTask.description = 'test description';
            (taskRepository.findOne as unknown as jest.Mock<Promise<Task>>).mockResolvedValue(testTask)

            expect(taskRepository.findOne).not.toHaveBeenCalled();

            const result = await tasksService.getTaskById(1, mockUser);

            expect(taskRepository.findOne).toHaveBeenCalledWith({ where: {
                id: 1,
                userId: mockUser.id } });
            expect(result).toEqual(testTask);
        })

        it('throws an error as task is not found', async () => {
            (taskRepository.findOne as unknown as jest.Mock<Promise<Task>>).mockResolvedValue(null)
            expect(taskRepository.findOne).not.toHaveBeenCalled();

            await expect(tasksService.getTaskById(1, mockUser)).rejects.toBeInstanceOf(NotFoundException)
        })
    })

    describe('createTask', () => {
        it('calls taskRepository.createTask() and succesfully create and return the task', async () => {
            const testTask = new Task();
            testTask.title = 'test title';
            testTask.description = 'test description';
            const createTaskDto: CreateTaskDto = {
                description: testTask.description,
                title: testTask.title
            };
            (taskRepository.createTask as unknown as jest.Mock<Promise<Task>>).mockResolvedValue(testTask)

            expect(taskRepository.createTask).not.toHaveBeenCalled();

            const result = await tasksService.createTask(createTaskDto, mockUser);

            expect(taskRepository.createTask).toHaveBeenCalledWith(createTaskDto, mockUser);
            expect(result).toEqual(testTask);
        })
    })

    describe('deleteTask', () => {
        it('calls taskRepository.deleteTask() to delete a task', async () => {
            (taskRepository.delete as unknown as jest.Mock<Promise<{ affected: number }>>).mockResolvedValue({ affected: 1 })

            expect(taskRepository.delete).not.toHaveBeenCalled();

            await tasksService.deleteTask(1, mockUser);

            expect(taskRepository.delete).toHaveBeenCalledWith({ id: 1, userId: mockUser.id });
        })

        it('throw an error as task could not be found', async () => {
            (taskRepository.delete as unknown as jest.Mock<Promise<{ affected: number }>>).mockResolvedValue({ affected: 0 })

            expect(taskRepository.delete).not.toHaveBeenCalled();
            await expect(tasksService.deleteTask(1, mockUser)).rejects.toThrow(NotFoundException);
        });
    })
})