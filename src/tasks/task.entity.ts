import { User } from "../auth/user.entity";
import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TaskStatus } from './task-status.enum';
import { ApiProperty } from "@nestjs/swagger";


@Entity()
export class Task extends BaseEntity {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    title: string;

    @ApiProperty()
    @Column()
    description: string;

    @ApiProperty({
        enum: TaskStatus
    })
    @Column()
    status: TaskStatus;

    @ManyToOne(() => User, user => user.tasks, { eager: false })
    user: User;

    @ApiProperty()
    @Column()
    userId: number;
}