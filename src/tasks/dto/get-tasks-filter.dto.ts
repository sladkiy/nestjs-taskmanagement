import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsIn, IsNotEmpty, IsOptional } from "class-validator";
import { TaskStatus } from "../task-status.enum";

export class GetTasksFilterDto {
    @ApiProperty({
        required: false,
        enum: [TaskStatus.OPEN, TaskStatus.IN_PROGRESS, TaskStatus.DONE]
    })
    @IsOptional()
    @IsIn([TaskStatus.OPEN, TaskStatus.IN_PROGRESS, TaskStatus.DONE])
    status?: TaskStatus;

    @ApiProperty({
        required: false,
    })
    @IsOptional()
    @IsNotEmpty()
    @Transform(search => search.trim())
    search?: string;
}